defmodule InpowerS3 do
  @moduledoc """
  Documentation for `InpowerS3`.
  """

  defp webroot do
    Application.fetch_env!(:inpower_s3, :webroot) || System.get_env("S3_WEBROOT")
  end

  defp bucket_name do
    Application.fetch_env!(:inpower_s3, :bucket) || System.get_env("S3_BUCKET")
  end

  defp http_client() do
    Application.fetch_env!(:inpower_s3, :bucket)
  end

  def public_storage() do
    dir = File.cwd!() <> "/priv/storage"
    File.chmod(dir, 0o755)

    unless File.exists?(dir) do
      File.mkdir!(dir)
    end

    dir
  end

  def random_name(length) do
    :crypto.strong_rand_bytes(length)
    |> Base.url_encode64()
    |> binary_part(0, length)
  end

  def get(object) do
    ExAws.S3.get_object(bucket_name(), object, [])
    |> ExAws.request!()
  end

  ####### original #########
  # def base64_to_upload(input, folder) do
  #   case :binary.match(input, ";base64,")  do #and :binary.match(input, "data:image/")
  #     {start, length} ->
  #       raw = :binary.part(input, start + length, byte_size(input) - start - length)
  #       name = random_name(64)
  #       extension = ".jpg"
  #       unique_filename = "/#{webroot()}/#{folder}/#{name}#{extension}"

  #       case ExAws.S3.put_object(bucket_name(), unique_filename, Base.decode64!(raw),
  #              acl: :public_read
  #            )
  #            |> ExAws.request!() do
  #         %{
  #           status_code: 200
  #         } ->
  #           {:ok, "https://inpower2.s3.amazonaws.com/" <> bucket_name() <> unique_filename}

  #         _ ->
  #           {:error}
  #       end
  #   end
  # end
#########################

######### for images ######
  def base64_to_upload("data:image/jpeg;base64,"<>raw, folder) do
    name = random_name(64)
    extension = ".jpg"
    unique_filename = "/#{webroot()}/#{folder}/#{name}#{extension}"

    case ExAws.S3.put_object(bucket_name(), unique_filename, Base.decode64!(raw),
          acl: :public_read
        )
        |> ExAws.request!() do
      %{
        status_code: 200
      } ->
        {:ok, "https://inpower2.s3.amazonaws.com/" <> bucket_name() <> unique_filename}

      _ ->
        {:error}
    end
  end

  def base64_to_upload("data:image/jpg;base64,"<>raw, folder) do
    name = random_name(64)
    extension = ".jpg"
    unique_filename = "/#{webroot()}/#{folder}/#{name}#{extension}"

    case ExAws.S3.put_object(bucket_name(), unique_filename, Base.decode64!(raw),
          acl: :public_read
        )
        |> ExAws.request!() do
      %{
        status_code: 200
      } ->
        {:ok, "https://inpower2.s3.amazonaws.com/" <> bucket_name() <> unique_filename}

      _ ->
        {:error}
    end
  end

  def base64_to_upload("data:image/png;base64,"<>raw, folder) do
        name = random_name(64)
        extension = ".jpg"
        unique_filename = "/#{webroot()}/#{folder}/#{name}#{extension}"

        case ExAws.S3.put_object(bucket_name(), unique_filename, Base.decode64!(raw),
               acl: :public_read
             )
             |> ExAws.request!() do
          %{
            status_code: 200
          } ->
            {:ok, "https://inpower2.s3.amazonaws.com/" <> bucket_name() <> unique_filename}

          _ ->
            {:error}
        end
      end

#################################

####### for video ######################

  def base64_to_upload("data:video/mp4;base64,"<>raw, folder) do
    name = random_name(64)
    extension = ".mp4"
    unique_filename = "/#{webroot()}/#{folder}/#{name}#{extension}"

    case ExAws.S3.put_object(bucket_name(), unique_filename, Base.decode64!(raw),
          acl: :public_read
        )
        |> ExAws.request!() do
      %{
        status_code: 200
      } ->
        {:ok, "https://inpower2.s3.amazonaws.com/" <> bucket_name() <> unique_filename}

      _ ->
        {:error}
    end
  end

  def base64_to_upload("data:video/mov;base64,"<>raw, folder) do
    name = random_name(64)
    extension = ".mp4"
    unique_filename = "/#{webroot()}/#{folder}/#{name}#{extension}"

    case ExAws.S3.put_object(bucket_name(), unique_filename, Base.decode64!(raw),
          acl: :public_read
        )
        |> ExAws.request!() do
      %{
        status_code: 200
      } ->
        {:ok, "https://inpower2.s3.amazonaws.com/" <> bucket_name() <> unique_filename}

      _ ->
        {:error}
    end
  end

  def base64_to_upload("data:video/mkv;base64,"<>raw, folder) do
    name = random_name(64)
    extension = ".mp4"
    unique_filename = "/#{webroot()}/#{folder}/#{name}#{extension}"

    case ExAws.S3.put_object(bucket_name(), unique_filename, Base.decode64!(raw),
          acl: :public_read
        )
        |> ExAws.request!() do
      %{
        status_code: 200
      } ->
        {:ok, "https://inpower2.s3.amazonaws.com/" <> bucket_name() <> unique_filename}

      _ ->
        {:error}
    end
  end

  def base64_to_upload("data:video/avi;base64,"<>raw, folder) do
    name = random_name(64)
    extension = ".mp4"
    unique_filename = "/#{webroot()}/#{folder}/#{name}#{extension}"

    case ExAws.S3.put_object(bucket_name(), unique_filename, Base.decode64!(raw),
          acl: :public_read
        )
        |> ExAws.request!() do
      %{
        status_code: 200
      } ->
        {:ok, "https://inpower2.s3.amazonaws.com/" <> bucket_name() <> unique_filename}

      _ ->
        {:error}
    end
  end

  def base64_to_upload("data:video/wmv;base64,"<>raw, folder) do
    name = random_name(64)
    extension = ".mp4"
    unique_filename = "/#{webroot()}/#{folder}/#{name}#{extension}"

    case ExAws.S3.put_object(bucket_name(), unique_filename, Base.decode64!(raw),
          acl: :public_read
        )
        |> ExAws.request!() do
      %{
        status_code: 200
      } ->
        {:ok, "https://inpower2.s3.amazonaws.com/" <> bucket_name() <> unique_filename}

      _ ->
        {:error}
    end
  end
################################
  def validate_video(file) do
    ~w(.mp4) |> Enum.member?(Path.extname(file.file_name))
  end

  @doc """
  validate accepted image format
  """
  def validate_audio(file) do
    ~w(.mp3) |> Enum.member?(Path.extname(file.file_name))
  end

  @doc """
  check and create directory if it doesn't exist
  """

  defp prepare_dir!(dir) do
    if(File.exists?(dir) == false) do
      File.mkdir_p(dir)
    end
  end
end
