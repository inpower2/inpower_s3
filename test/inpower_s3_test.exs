defmodule InpowerS3Test do
  use ExUnit.Case
  doctest InpowerS3

  test "base64 image upload" do
    upload =
      InpowerS3.base64_to_upload(
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+P+/HgAFhAJ/wlseKgAAAABJRU5ErkJggg==",
        "folder"
      )

    assert {:ok, _} = upload
  end
end
