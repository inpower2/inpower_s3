# InpowerS3

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `inpower_s3` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:inpower_s3, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/inpower_s3](https://hexdocs.pm/inpower_s3).

#Config 
```elixir

config :ex_aws,
  debug_requests: true,
  access_key_id: "Test",
  secret_access_key: "Key",
  s3: [
    scheme: "https://",
    host: "inpower2.s3.amazonaws.com",
    region: "us-east-2"
  ]

config :inpower_s3,
  bucket: "bucket",
  region: "us-east-2",
  webroot: "service_name_space"



```
```elixir 
 InpowerS3.base64_to_upload(
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+P+/HgAFhAJ/wlseKgAAAABJRU5ErkJggg==",
  "folder" )

```
