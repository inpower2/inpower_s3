use Mix.Config

# Upload to S3
config :ex_aws,
  debug_requests: true,
  access_key_id: System.get_env("ACCESS_KEY_ID"),
  secret_access_key: System.get_env("AWS_SECRECT_ACCESS_KEY_ID"),
  s3: [
    scheme: "https://",
    host: System.get_env("AWS_S3_HOST"),
    region: System.get_env("AWS_REGION")
  ]

config :inpower_s3,
  bucket: System.get_env("S3_BUCKET"),
  region: System.get_env("S3_REGION"),
  webroot: System.get_env("S3_WEBROOT")
